package com.sergio.testrappi.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Before
import org.junit.Test

import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class MoviesViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    @Mock
    lateinit var viewModel: MoviesViewModel

    @Before
    fun setup() {
        //in this method we will create the class instance to test (MainViewModel) and we will initialize all mocked elements with the function:
        MockitoAnnotations.initMocks(this)
        this.viewModel = MoviesViewModel()
    }

    @Test
    fun getMoviesPopular_positiveResponse() {
        viewModel.moviesPopular.observeForever{}
        assert(viewModel.moviesPopular.value==null)
    }
}