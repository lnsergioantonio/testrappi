package com.sergio.testrappi.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

val Context.networkInfo : NetworkInfo? get() = (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo

class NetworkHandler(private val context: Context){
    val isConnected get() = context.networkInfo?.isConnected
}

