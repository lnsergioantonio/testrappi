package com.sergio.testrappi.ui.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.sergio.testrappi.R
import kotlinx.android.synthetic.main.activity_details.*

class Details : AppCompatActivity() {
    private var average: Double = 0.0;
    private var title:String=""
    private var releaseDate:String=""
    private var overview:String=""
    private var posterPath:String=""
    private var backdropPath:String=""
    private var urlBase: String = "https://image.tmdb.org/t/p/w500/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        intent.extras?.let{
            title = it.getString("title","")
            releaseDate = it.getString("releaseDate","")
            overview = it.getString("overview","")
            posterPath = it.getString("posterPath","")
            backdropPath = it.getString("backdropPath","")
            average = it.getDouble("average",0.0)
        }

        detailsLabelAverage.text = average.toString()
        detailsLabelOverview.text = overview
        detailsLabelRelease.text = releaseDate
        detailsLabelTitle.text = title

        Glide.with(this).load(urlBase+backdropPath).into(detailsImageBackground)
        Glide.with(this).load(urlBase+posterPath).into(detailsImageProfile)
    }
}
