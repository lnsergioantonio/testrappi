package com.sergio.testrappi.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.sergio.testrappi.data.MoviesRepository
import com.sergio.testrappi.data.network.entities.EntityMovie
import kotlinx.coroutines.Dispatchers

class MoviesViewModel : ViewModel(){
    private val moviesRepository: MoviesRepository = MoviesRepository()
    private val moviesPopularLiveData = MutableLiveData<MutableList<EntityMovie>>()
    private val moviesRatedLiveData = MutableLiveData<MutableList<EntityMovie>>()
    private val moviesUpcomingLiveData = MutableLiveData<MutableList<EntityMovie>>()

    var moviesPopular = liveData(Dispatchers.Default){
        val movies: MutableList<EntityMovie>? = moviesRepository.getMoviesPopular()
        moviesPopularLiveData.postValue(movies)
        emit(moviesPopularLiveData)
        /*try {
        }catch (e:Exception){
            emit(null)
        }*/
    }

    var moviesRated= liveData(Dispatchers.Default){
        val movies: MutableList<EntityMovie>? = moviesRepository.getMoviesRated()
        moviesRatedLiveData.postValue(movies)
        emit(moviesRatedLiveData)
    }

    var moviesUpcoming= liveData(Dispatchers.Default){
        val movies: MutableList<EntityMovie>? = moviesRepository.getMoviesUpcoming()
        moviesUpcomingLiveData.postValue(movies)
        emit(moviesUpcomingLiveData)
    }

    fun filterMovie(strMovie: String) = liveData(Dispatchers.Default){
        val movies = moviesRepository.searchMovie("")
        moviesPopularLiveData.postValue(movies)
        emit(moviesPopularLiveData.value)
    }

    //fun getPopularMovies(){
        /*viewModelScope.launch {
            var popularMovies : MutableList<EntityMovie> ? =null
            withContext(Dispatchers.Default){
                popularMovies = moviesRepository.getMoviesPopular()
            }
            moviesLiveData.value = popularMovies
        }*/
        /*
        * scope.launch {
            val popularMovies = moviesRepository.getMoviesPopular()
            moviesLiveData.postValue(popularMovies)
        }*/
    //}

    //fun cancelRequest() = coroutineWithContext.cancel()
}