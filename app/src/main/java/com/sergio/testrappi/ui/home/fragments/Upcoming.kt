package com.sergio.testrappi.ui.home.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.sergio.testrappi.R
import com.sergio.testrappi.ui.home.MoviesViewModel
import com.sergio.testrappi.ui.home.fragments.adapters.MovieAdapter
import kotlinx.android.synthetic.main.fragment_upcoming.*

class Upcoming : Fragment() {
    private var urlBase: String = "https://image.tmdb.org/t/p/w500/"
    private lateinit var movieViewModel: MoviesViewModel
    private var listener: OnFragmentInteractionListener? = null
    private val adapter = MovieAdapter(urlBase,listener = {movie ->
        this.listener?.onFragmentInteraction(movie)
    })
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieViewModel = activity?.run {
            ViewModelProviders.of(this).get(MoviesViewModel::class.java)
        }?:throw Exception("Invalid Activity")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_upcoming, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        frUpcomingRecycler.adapter = adapter
        movieViewModel.moviesUpcoming.observe(this, Observer {movies->
            adapter.setList(movies.value)
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
