package com.sergio.testrappi.ui.home.fragments

import com.sergio.testrappi.data.network.entities.EntityMovie

interface OnFragmentInteractionListener {
    fun onFragmentInteraction(movie:EntityMovie)
}