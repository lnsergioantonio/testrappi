package com.sergio.testrappi.ui.home.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sergio.testrappi.R
import com.sergio.testrappi.data.network.entities.EntityMovie
import kotlinx.android.synthetic.main.item_popular.view.*

class MovieAdapter(
    val urlBaseImage:String,
    val listener: (EntityMovie) -> Unit
): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var listMovies:MutableList<EntityMovie> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_popular,parent,false)
        return MovieViewHolder(view)
    }
    fun setList(mMovies: MutableList<EntityMovie>?){
        this.listMovies= mMovies!!
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return listMovies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val mMovie = listMovies[position]
        holder.bind(mMovie,urlBaseImage)
        holder.itemView.setOnClickListener { listener(mMovie) }
    }

    class MovieViewHolder(view:View) : RecyclerView.ViewHolder(view){
        fun bind(movie:EntityMovie,urlBaseImage: String){
            itemView.frPopularLabelTitle.text       = movie.title
            itemView.frPopularLabelPopularity.text  = movie.popularity.toString()
            itemView.frPopularLabelRelease.text     = movie.releaseDate

            Glide
                .with(itemView.context)
                .load(urlBaseImage+movie.posterPath)
                .into(itemView.frPopularImage)
        }
    }
}