package com.sergio.testrappi.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.core.view.MenuItemCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.sergio.testrappi.R
import com.sergio.testrappi.data.network.entities.EntityMovie
import com.sergio.testrappi.ui.details.Details
import com.sergio.testrappi.ui.home.fragments.OnFragmentInteractionListener
import com.sergio.testrappi.ui.home.fragments.Popular
import kotlinx.android.synthetic.main.activity_main.*

class Home : AppCompatActivity(), OnFragmentInteractionListener {
    override fun onFragmentInteraction(movie: EntityMovie) {
        val bundle = Bundle().apply {
            putString("title",movie.title)
            putString("releaseDate",movie.releaseDate)
            putString("overview",movie.overview)
            putString("posterPath",movie.posterPath)
            putString("backdropPath",movie.backdropPath)
            putDouble("average",movie.voteAverage)
        }
        val intent = Intent(this, Details::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val navController: NavController = Navigation.findNavController(this, R.id.fragment_navigation)
        setupWithNavController(bottomNavigation,navController)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        val menuItem = menu?.findItem(R.id.action_search)

        val searchView : SearchView = menuItem?.actionView as SearchView
        search(searchView)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }

    private fun search(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { Popular.newInstance().searchMovie(it) }
                return true
            }
        })
    }
}