package com.sergio.testrappi.data

import android.util.Log
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

open class BaseRepository {
    suspend fun <T : Any> safeApiCall(call : suspend()-> Response<T>, error : String) :  T?{
        val result = safeApiResult(call, error)
        var output : T? = null
        when(result){
            is Output.Success ->
                output = result.output
            is Output.Error -> Log.e("Error", "The $error and the ${result.exception}")
        }
        return output

    }
    private suspend fun<T : Any> safeApiResult(call: suspend()-> Response<T>, error: String) : Output<T>{
        return try {
                    val response = call.invoke()
                    if (response.isSuccessful)
                        Output.Success(response.body()!!)
                    else
                        Output.Error("OOps .. Something went wrong due to  $response.code()")
                } catch (se3: SocketTimeoutException) {
                    Output.Error("OOps .. SocketTimeoutException|Error Something went wrong due to  ${se3.message}")
                } catch (e: Exception) {
                    Output.Error("OOps .. Exception|Error Something went wrong due to  ${e.message}")
                }
    }
}