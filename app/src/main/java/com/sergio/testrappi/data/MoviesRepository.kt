package com.sergio.testrappi.data

import com.sergio.testrappi.data.network.AppAppi
import com.sergio.testrappi.data.network.entities.EntityMovie

class MoviesRepository: BaseRepository() {
    companion object{
        private val moviesApi by lazy {
            return@lazy AppAppi.apiService
        }
    }

    suspend fun getMoviesPopular(): MutableList<EntityMovie>?{
        return safeApiCall(
            call = {
                moviesApi.fetchtMovies("popularity.desc")
            },
            error = "Error fetch movies"
        )?.results?.toMutableList()
    }

    suspend fun getMoviesRated(): MutableList<EntityMovie>?{
        return safeApiCall(
            call = {
                moviesApi.fetchtMovies("vote_average.desc")
            },
            error = "Error fetch movies"
        )?.results?.toMutableList()
    }

    suspend fun getMoviesUpcoming(): MutableList<EntityMovie>?{
        return safeApiCall(
            call = {
                moviesApi.fetchtMovies("release_date.desc")
            },
            error = "Error fetch movies"
        )?.results?.toMutableList()
    }

    suspend fun searchMovie(strMovie:String): MutableList<EntityMovie>?{
        return safeApiCall(
            call = {
                moviesApi.searchMovies("4daa61817a9202ea12cf295d6d54418e",strMovie)
            },
            error = "Error fetch movies"
        )?.results?.toMutableList()
    }
}