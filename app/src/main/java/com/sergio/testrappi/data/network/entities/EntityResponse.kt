package com.sergio.testrappi.data.network.entities

import com.google.gson.annotations.SerializedName

class EntityResponse(
    @SerializedName("page")
    val page:Int,
    @SerializedName("total_results")
    val totalResults:Int,
    @SerializedName("total_pages")
    val totalPages:Int,
    @SerializedName("results")
    val results:ArrayList<EntityMovie>
)