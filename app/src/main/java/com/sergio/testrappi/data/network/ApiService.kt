package com.sergio.testrappi.data.network

import com.sergio.testrappi.data.network.entities.EntityResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {
    @Headers(
        "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0ZGFhNjE4MTdhOTIwMmVhMTJjZjI5NWQ2ZDU0NDE4ZSIsInN1YiI6IjVkMzc1ZDVmMzU4MTFkMDAxNGJjYWFhZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.v1tGh9a71pywtvhMe6OHBf_Hsy3CDOHQDR7YSFKZd5s"
    )
    @GET("4/discover/movie")
    suspend fun fetchtMovies(@Query("sort_by") sort: String?): Response<EntityResponse>

    @GET("3/search/movie")
    suspend fun searchMovies(@Query("api_key") api_key:String,@Query("query") query:String): Response<EntityResponse>
}